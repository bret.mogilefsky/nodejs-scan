package main

import (
	"bufio"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/common/search"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/plugin"
)

const (
	artifactName    = "gl-sast-report.json"
	flagTargetDir   = "target-dir"
	flagArtifactDir = "artifact-dir"
	flagRulesPath   = "rules"

	babelPresets = "stage-0,react,flow"
	pathHome     = "/home/node"
	pathBabelCli = "./node_modules/.bin/babel"
	pathInDir    = "./in"
	pathOutDir   = "./out"

	toolID = "nodejs_scan"
)

// Run is inspired from command.Run of the analyzers/common library.
func Run() cli.Command {
	flags := []cli.Flag{
		cli.StringFlag{
			Name:   flagTargetDir,
			Usage:  "Target directory",
			EnvVar: "ANALYZER_TARGET_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagArtifactDir,
			Usage:  "Artifact directory",
			EnvVar: "ANALYZER_ARTIFACT_DIR,CI_PROJECT_DIR",
		},
		cli.StringFlag{
			Name:   flagRulesPath,
			Usage:  "Path of NodeJsScan XMLrules",
			EnvVar: "NODEJS_SCAN_RULES_FILE",
			Value:  "rules.xml",
		},
	}
	flags = append(flags, search.NewFlags()...)

	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a SAST artifact",
		Flags:   flags,
		Action: func(c *cli.Context) error {
			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errInvalidArgs
			}

			// search directory
			root, err := filepath.Abs(c.String(flagTargetDir))
			root = strings.TrimSuffix(root, "/") + "/"
			if err != nil {
				return err
			}

			// search
			searchOpts := search.NewOptions(c)
			matchPath, err := search.New(plugin.Match, searchOpts).Run(root)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, "Found project in "+matchPath)

			// load rules
			pathRules := c.String(flagRulesPath)
			rules, err := loadRules(pathRules)
			if err != nil {
				return err
			}
			fmt.Fprintln(c.App.Writer, len(rules), "rules loaded") // DEBUG

			// create symlink
			// babel-cli requires the input files to be under the home directory
			// (where node modules have been installed) else it can't load the presets.
			if err := os.Symlink(root, pathInDir); err != nil {
				return err
			}

			// build files list
			relPaths := []string{}
			inPaths := []string{}
			outPaths := []string{}
			err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if info.IsDir() {
					switch info.Name() {
					case "node_modules", "vendor", "spec", "test":
						return filepath.SkipDir
					default:
						// OK
					}
				} else {
					if ok, _ := plugin.Match(path, info); ok {
						relPath := strings.Replace(path, root, "", 1)
						relPaths = append(relPaths, relPath)
						inPaths = append(inPaths, filepath.Join(pathInDir, relPath))
						outPaths = append(outPaths, filepath.Join(pathOutDir, pathInDir, relPath))
					}
				}
				return nil
			})
			if err != nil {
				return err
			}

			// remove comments using Babel
			args := []string{
				"--presets", babelPresets,
				"--no-comments",
				"--retain-lines",
				"--no-babelrc",
				"-d", pathOutDir,
			}
			args = append(args, inPaths...)

			cmd := exec.Command(pathBabelCli, args...)
			cmd.Env = os.Environ()
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Dir = pathHome
			if err := cmd.Run(); err != nil {
				return err
			}

			// apply rules to find issues
			issues := []issue.Issue{}
			for i, outPath := range outPaths {
				issues2, err := scanFile(outPath, relPaths[i], rules)
				if err != nil {
					return err
				}
				issues = append(issues, issues2...)
			}

			// write indented JSON to artifact
			artifactPath := filepath.Join(c.String(flagArtifactDir), artifactName)
			f2, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
			if err != nil {
				return err
			}
			defer f2.Close()
			enc := json.NewEncoder(f2)
			enc.SetIndent("", "  ")
			return enc.Encode(issues)
		},
	}
}

func loadRules(path string) ([]Rule, error) {
	set := RuleSet{}
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	err = xml.NewDecoder(f).Decode(&set)
	return set.Rules()
}

func scanFile(path, relPath string, rules []Rule) ([]issue.Issue, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	var lineNumber int
	var issues []issue.Issue
	for scanner.Scan() {
		lineNumber++
		line := scanner.Text()
		for _, rule := range rules {
			if rule.Match(line) {
				def := rule.Definition()
				issues = append(issues, issue.Issue{
					Tool:        toolID,
					Category:    issue.CategorySast,
					Name:        def.Name,
					Message:     def.Name,
					Description: def.Description,
					Location: issue.Location{
						File:      relPath,
						LineStart: lineNumber,
					},
					CompareKey: def.Name + ":" + line,
				})
			}
		}
	}
	return issues, err
}
